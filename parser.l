%{
/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "parser-private.h"

%}

STRING_CHARS [[:print:]]{-}[\"]
WS           [[:space:]]{-}[\r]{-}[\n]
NUM          [[:digit:]]+

%option noyywrap nounput noinput debug yylineno

%%

\r\n|\n                                 return NEWLINE;
REM{WS}*[[:print:]]*                    return REM;
\"{STRING_CHARS}*\"                     R(STRING);
{NUM}                                   R(INTEGER);
{NUM}\.{NUM}                            R(REAL);
      
AND                                     return AND;
OR                                      return OR;
NOT|"!"                                 return NOT;
"<>"|"<>"                               return NEQ;
"<="                                    return LEQ;
">="                                    return GEQ;
"<"                                     return LT;
">"                                     return GT;
"="                                     return EQ;
"^"|"**"                                return POW;
"+"                                     return ADD;
"-"                                     return SUB;
"*"                                     return MULT;
"/"                                     return DIV;
"%"                                     return MOD;
"("                                     return LPAREN;
")"                                     return RPAREN;
DIM                                     return DIM;
END                                     return END;
LET                                     return LET;
POKE                                    return POKE;
RESTORE                                 return RESTORE;
RUN                                     return RUN;
STOP                                    return STOP;
SYS                                     return SYS;
WAIT                                    return WAIT;
FOR                                     return FOR;
TO                                      return TO;
STEP                                    return STEP;
NEXT                                    return NEXT;
GOTO                                    return GOTO;
GOSUB                                   return GOSUB;
RETURN                                  return RETURN;
IF                                      return IF;
THEN                                    return THEN;
OPEN                                    return OPEN;
AS                                      return AS;
CLOSE                                   return CLOSE;
READ                                    return READ;
INPUT                                   return INPUT;
OUTPUT                                  return OUTPUT;
DATA                                    return DATA;
PRINT                                   return PRINT;
DONE                                    return DONE;

[:.,;]                                  return yytext[0];

[[:alpha:]][[:alnum:]_]*[$%]?           R(ID);
%%
