#!/bin/sh

PARSER=${1:-"../basicc"}

for i in ./*.bas; do
    echo "=====[ $i ]====="
     $PARSER $i
     if [ $? -ne 0 ]; then
	 echo "test $i failed!"
	 exit $?
     fi
done

