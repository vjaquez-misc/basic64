%{
/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include  "parser-private.h"
%}

%debug
%error-verbose
%no-lines

%union {
	long integer;
	double real;
	unsigned char byte;
	const char *string;
        AstNode *node;
}

%token <integer> INTEGER
%token <real>    REAL
%token <string>  STRING

%token <string> ID
%token NEWLINE
%token REM
%token DIM END LET POKE RESTORE RUN STOP SYS WAIT
%token FOR TO STEP NEXT DONE
%token GOTO GOSUB RETURN
%token IF THEN
%token OPEN AS CLOSE READ
%token INPUT OUTPUT DATA PRINT

%token ADD SUB
%token MULT DIV MOD AND OR NEQ LEQ GEQ LT GT EQ NOT
%token LPAREN RPAREN

%left ADD SUB
%left MULT DIV MOD
%left AND OR
%left NEQ LEQ GEQ LT GT EQ
%right NOT
%right POW

%type <node>    line
%type <node>    constant ident
%type <real>    knumber
%type <integer> unary_operator

%start lines

%%

lines
        : line
        | lines line
;

line
        : INTEGER statements NEWLINE
	{
		printf("%ld - \n", $1);
		/* ast_line_append ((AstLine *) ast_get_current_node (), */
		/* 		 (AstNode *) ast_line_new ($1, $<string>2)); */
	}
;

statements
	: statement ':' statements
	| statement
;

statement
	: CLOSE '#' INTEGER
	| DATA constant_list
	| DIM ident LPAREN integer_list RPAREN
	| END
	| FOR ident EQ expression TO expression
	| FOR ident EQ expression TO expression STEP INTEGER
	| GOTO expression
	| GOSUB expression
	| IF expression THEN statement
	| INPUT id_list
	| INPUT STRING ',' id_list
	| INPUT '#' INTEGER ',' id_list
	| LET ident EQ expression
	| NEXT id_list
	| OPEN value FOR access AS '#' INTEGER
	| POKE value_list
	| PRINT print_list
	| PRINT '#' INTEGER ',' print_list
	| READ id_list
	| RETURN
	| RESTORE
	| RUN
	| STOP
	| SYS value
	| WAIT value_list
	| REM
	| DONE
;

access
	: INPUT
	| OUTPUT
;

id_list
	: ident ',' id_list
	| ident
;

value_list
	: value ',' value_list
	| value
;

constant_list
	: constant ',' constant_list
	| constant
;

integer_list
	: INTEGER ',' integer_list
	| INTEGER
;

expression_list
	: expression ',' expression_list
	| expression
;

print_list
	: expression ';' print_list
	| expression ',' print_list
	| expression
;

expression
	: logical_or_expression
	| NOT logical_or_expression
;

logical_or_expression
	: logical_and_expression
	| logical_or_expression OR logical_and_expression

logical_and_expression
	: equality_expression
	| logical_and_expression AND equality_expression
;

equality_expression
	: relational_expression
	| equality_expression EQ relational_expression
	| equality_expression NEQ relational_expression
;

relational_expression
	: additive_expression
	| relational_expression GT additive_expression
	| relational_expression LT additive_expression
	| relational_expression LEQ additive_expression
	| relational_expression GEQ additive_expression
;

additive_expression
	: multiplicative_expression
	| additive_expression ADD multiplicative_expression
	| additive_expression SUB multiplicative_expression
;

multiplicative_expression
	: value
	| multiplicative_expression MULT value
	| multiplicative_expression DIV value
	| multiplicative_expression MOD value
;

value
	: constant
	| LPAREN expression RPAREN
	| ident
	| ident LPAREN expression_list RPAREN
;

ident
	: ID
	{
		$$ = ast_identifier_new($1);
	}
;

constant
        : knumber
        {
		$$ = ast_literal_number_new($1);
        }
	| unary_operator knumber
	{
		$$ = ast_literal_number_new($1 * $2);
	}
	| STRING
	{
		$$ = ast_literal_string_new($1);
	}
;

knumber
	: INTEGER { $$ = (double) $1; }
	| REAL
;

unary_operator
	: ADD { $$ = 1;  }
	| SUB { $$ = -1; }
;

%%

void
yaccset_debug(bool debug)
{
	if (debug)
		yydebug = 1;
}
