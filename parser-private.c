/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <stdlib.h>
#include <errno.h>

#include "parser-private.h"
#include "log.h"

int yyget_lineno (void);

void
yyerror (char const *msg)
{
	pr_err ("%s at line %d", msg, yyget_lineno ());
}

int yyparse (void);
void yyset_debug (int  bdebug);
struct yy_scan_state *yy_scan_bytes (const char *base, int size);

int
parse (const char *script, int size, bool lex_debug, bool yacc_debug)
{
	if (!script || size < 0)
		return 3;

	yyset_debug (lex_debug);
	yaccset_debug (yacc_debug);
	yy_scan_bytes (script, size);
	return yyparse ();
}

void
yyget_INTEGER (YYSTYPE *outval, const char *text, size_t len)
{
	errno = 0;
	outval->integer = strtol (text, NULL, 0);
	if (errno != 0) {
		pr_warning ("overflow");
		outval->integer = 0;
	}
}

void
yyget_REAL (YYSTYPE *outval, const char *text, size_t len)
{
	errno = 0;
	outval->real = strtod (text, NULL);
	if (errno != 0) {
		pr_warning ("overflow");
		outval->real = 0;
	}
}

void
yyget_STRING (YYSTYPE *outval, const char *text, size_t len)
{
	outval->string = text;
}
