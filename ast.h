/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _AST_H_
#define _AST_H_

typedef struct _AstVistor AstVisitor;

typedef struct _AstNode AstNode;
typedef struct _AstNodeClass AstNodeClass;

#define STATEMENT_NODE_LIST(V)			\
	V(Line, LINE, line)			\
	V(ExpStmnt, EXPSTMNT, exp_stmnt)	\
	V(IfStmnt, IFSTMNT, if_stmnt)		\
	V(RetStmnt, RETSTMNT, ret_stmnt)	\
	V(ForStmnt, FORSTMNT, for_stmnt)

#define EXPRESSION_NODE_LIST(V)				\
	V(Function, FUNCTION, function)                 \
	V(Conditional, CONDITIONAL, conditional)	\
	V(Variable, VARIABLE, variable)                 \
	V(Literal, LITERAL, literal)			\
	V(ArrayLiteral, ARRAYLITERAL, array_literal)	\
	V(Assignment, ASSIGNMENT, assignment)		\
	V(Call, CALL, call)				\
	V(BinOp, BINOP, bin_op)				\
	V(CompOp, COMPOP, comp_op)

#define AST_NODE_LIST(V)				\
	V(Identifier, IDENTIFIER, identifier)		\
	V(Declaration, DECLARATION, declaration)	\
	STATEMENT_NODE_LIST(V)				\
	EXPRESSION_NODE_LIST(V)

#define DECLARE_TYPE_ENUM(type,enm,camel) AST_TYPE_##enm,
enum AstType {
	AST_NODE_LIST(DECLARE_TYPE_ENUM)
	AST_TYPE_INVALID = -1
};
#undef DECLARE_TYPE_ENUM

struct _AstNodeClass {
	void (*destroy) (AstNode *self);
	void (*accept)  (AstNode *self, AstVisitor *visitor);
};

struct _AstNode {
	AstNodeClass class;
	enum AstType type;
};

#define DEF_FORWARD_DECLARATION(type,enm,camel)		       \
	typedef struct _Ast##type Ast##type;		       \
	typedef struct _Ast##type##Private Ast##type##Private; \
	struct _Ast##type {				       \
		AstNode parent;				       \
		Ast##type##Private *priv;		       \
	};
AST_NODE_LIST(DEF_FORWARD_DECLARATION)
#undef DEF_FORWARD_DECLARATION

void ast_node_accept(AstNode *self, AstVisitor *visitor);
void ast_node_free(AstNode *self);

enum AstLiteralType {
	AST_LITERAL_NUMBER,
	AST_LITERAL_STRING,
	AST_LITERAL_INVALID = -1
};
AstNode *ast_literal_string_new(const char *value);
AstNode *ast_literal_number_new(double value);

AstNode *ast_identifier_new(const char *name);

#endif /* _AST_H_ */
