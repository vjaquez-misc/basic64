/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _PARSER_PRIVATE_H_
#define _PARSER_PRIVATE_H_

#include <stddef.h> /* for size_t */
#include <stdbool.h>

#include "ast.h"        /* for yyval */
#include "parser.tab.h" /* for YYSTYPE */

int yylex (void);
void yyerror (char const *msg);
void yaccset_debug(bool debug);

int parse (const char *script, int len, bool lex_debug, bool yacc_debug);

#define R(type)					\
{						\
        yyget_##type(&yylval, yytext, yyleng);	\
        return  type;				\
} while (0)

void yyget_INTEGER (YYSTYPE *outval, const char *text, size_t len);
void yyget_REAL    (YYSTYPE *outval, const char *text, size_t len);
void yyget_STRING  (YYSTYPE *outval, const char *text, size_t len);
#define yyget_ID yyget_STRING

#endif /* _PARSER_PRIVATE_H_ */
