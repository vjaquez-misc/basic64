/*
 * Copyright (C) 2009-2010 Felipe Contreras
 *
 * Author: Felipe Contreras <felipe.contreras@gmail.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "log.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

void
pr_helper(unsigned int level,
	  const char *file,
	  const char *function,
	  unsigned int line,
	  const char *fmt,
	  ...)
{
	char *tmp;
	va_list args;

	va_start (args, fmt);

	if (vasprintf (&tmp, fmt, args) < 0)
		goto leave;

	if (level <= 1)
		fprintf (stderr, "%s: %s\n", function, tmp);
	else if (level == 2)
		printf ("%s:%s(%u): %s\n", file, function, line, tmp);
#ifdef DEBUG
	else if (level == 3)
		printf ("%s: %s\n", function, tmp);
	else if (level == 4)
		printf ("%s:%s(%u): %s\n", file, function, line, tmp);
#endif

	free (tmp);

leave:
	va_end (args);
}
