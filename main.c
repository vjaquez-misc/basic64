/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "parser-private.h"
#include "log.h"

bool flag_lex_debug;
bool flag_yacc_debug;
bool flag_dump_ast;

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

static char *
read_file (const char *filename, long *size)
{
	FILE *fh;
	char *str = NULL;
	long s = -1, i;

	fh = fopen (filename, "rb");
	if (!fh) {
		pr_err ("Could not open file %s: %s", filename, strerror (errno));
		goto bailout;
	}

	fseek (fh, 0, SEEK_END);
	s = ftell (fh);
	rewind (fh);

	str = malloc ((s + 1) * sizeof (char));
	str[s] = '\0';
	for (i = 0; i < s;) {
		i += fread (&str[i], 1, s - i, fh);
	}
	fclose (fh);

bailout:
	if (size)
		*size = s;

	return str;
}

static bool
compile (char *script, long size)
{
	if (script && size == -1)
		size = strlen(script);

	int res = parse (script, size, flag_lex_debug, flag_yacc_debug);
	/* if (res == 0 && flag_dump_ast) */
		/* ast_dump (); */

	/* ast_free (); */

	return (res == 0);
}

struct option {
	const char* name;
	bool *flag;
} options[] = {
	{ "--lex_debug",  &flag_lex_debug,  },
	{ "--yacc_debug", &flag_yacc_debug, },
	{ "--dump_ast",   &flag_dump_ast,   },
};

static void
set_flags (int *argc, char **argv)
{
	int i;
	unsigned j;

	for (i = 1; i < *argc; i++) {
		const char *arg = argv[i];
		for (j = 0; j < ARRAY_SIZE (options); j++) {
			if (strcmp (arg, options[j].name) == 0) {
				*options[j].flag = true;
				argv[i] = NULL;
			}
		}
	}

	j = 1;
	for (i = 1; i < *argc; i++) {
		if (argv[i] != NULL)
			argv[j++] = argv[i];
	}
	*argc = j;
}

static bool
execute (int argc, char **argv)
{
	int i;

	set_flags (&argc, argv);

	bool is_shell = (argc == 1);

	for (i = 1; i < argc; i++) {
		const char *arg = argv[i];

		if (strcmp (arg, "--shell") == 0) {
			is_shell = true;
		} else if (strcmp (arg, "-e") == 0 && i + 1 < argc) {
			if (!compile (argv[++i], -1))
				return false;
		} else {
			long size = -1;
			char *script = read_file (arg, &size);
			bool ok = compile (script, size);
			free (script);
			if (!ok)
				return false;
		}
	}

	if (is_shell) {
		pr_info ("shell unimplemented");
		return false;
	}

	return true;
}

int
main (int argc, char **argv)
{
	return execute (argc, argv) ? 0 : -1;
}
