/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Contact: Victor Jaquez <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ast.h"
#include "log.h"

AstNode *root;
AstNode *current;

#define AST_NODE(n)         ((AstNode *) n)
#define AST_NODE_CLASS(n)   ((AstNodeClass *) n)
#define AST_NODE_TYPE(n)    AST_NODE(n)->type
#define AST_NODE_DESTROY(n) AST_NODE_CLASS(n)->destroy

#define PRIV(n) (n)->priv

AstNode *
ast_node_new(enum AstType type)
{
	AstNode *node = calloc(1, sizeof(AstNode));
	node->type = type;
	return node;
}

void
ast_node_visit(AstNode *node, AstVisitor *visitor)
{
	AstNodeClass *class = AST_NODE_CLASS(node);
	if (class->accept)
		class->accept(node, visitor);
}

void
ast_node_free(AstNode *node)
{
	AstNodeClass *class = AST_NODE_CLASS(node);
	if (class->destroy)
		class->destroy(node);
	free(node);
}

struct _AstLiteralPrivate {
	enum AstLiteralType type;
	union {
		double num;
		char *str;
	} value;
};

static void
ast_literal_free(AstNode *node)
{
	AstLiteral *self = (AstLiteral *) node;
	AstLiteralPrivate *priv = PRIV(self);
	if (priv->type == AST_LITERAL_STRING)
		free(priv->value.str);
	free(priv);
}

static inline AstLiteral *
ast_literal_new(enum AstLiteralType type)
{
	AstLiteral *node = calloc(1, sizeof(AstLiteral));
	AST_NODE_TYPE(node) = AST_TYPE_LITERAL;

	AstLiteralPrivate *priv = calloc(1, sizeof(AstLiteralPrivate));
	PRIV(node) = priv;
	PRIV(node)->type = type;

	AST_NODE_DESTROY(node) = ast_literal_free;

	return node;
}

AstNode *
ast_literal_string_new(const char *value) {
	AstLiteral *node = ast_literal_new(AST_LITERAL_STRING);
	if (value && value[0])
		PRIV(node)->value.str = strdup(value);

	return AST_NODE(node);
}

AstNode *
ast_literal_number_new(double value) {
	AstLiteral *node = ast_literal_new(AST_LITERAL_NUMBER);
	PRIV(node)->value.num = value;
	return AST_NODE(node);
}

struct _AstIdentifierPrivate {
	char *name;
};

static void
ast_identifier_free(AstNode *node)
{
	AstIdentifier *value = (AstIdentifier *) node;
	free(PRIV(value)->name);
	free(PRIV(value));
}

AstNode *
ast_identifier_new(const char *name)
{
	AstIdentifier *var = calloc(1, sizeof(AstIdentifier));
	AST_NODE_TYPE(var) = AST_TYPE_IDENTIFIER;

	AstIdentifierPrivate *priv = calloc(1, sizeof(AstIdentifierPrivate));
	PRIV(var) = priv;
	if (name && *name)
		PRIV(var)->name = strdup(name);
	/* @TODO:
	   a) search in the defined identifier list
	   b) if it doesn't exist add it
	   c) store the offset in the identifier list, not its name
	*/

	AST_NODE_DESTROY(var) = ast_identifier_free;

	return AST_NODE(var);
}

