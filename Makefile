CFLAGS := -ggdb -Wall -Wextra -Wno-unused-parameter -Wmissing-prototypes -ansi -std=c99

override CFLAGS += -D_GNU_SOURCE -DDEBUG

YFLAGS := --warnings=all --verbose -d
LFLAGS := --case-insensitive

basicc: parser.yy.o parser.tab.o parser-private.o main.o log.o ast.o
bins += basicc

.SECONDARY: parser.yy.c parser.tab.c parser.tab.h

.PHONE: check

%.o : %.c
	$(CC) $(CFLAGS) -MMD -o $@ -c $<

$(bins) :
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

%.yy.c : %.l %.tab.c
	$(LEX) $(LFLAGS) -o $@ $<

%.tab.c : %.y
	$(YACC) $(YFLAGS) -o $@ $<

check: $(bins)
	cd tests && ./tests.sh

clean:
	$(RM) -v $(bins) *.yy.[ch] *.tab.[ch] *.o *.d *.output

-include *.d
